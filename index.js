// console.log("Hello Zawarudo!");

// [Section] JSON Objects

/*
	- JSON stands for JavaScript Object Notation
	- JSON is also used in other programming languages hence the name JSON
	- Core JS has a built in JSON object that contains methods for parsing JSON objects and converting strings into JS objects.
	- JSON is used for serializing different data types.

		syntax:
			"propertyA": "valueA",
			"propertyB": "valueB"
*/

	// JSON Object
		// {
		// 	"city": "Quezon City",
		// 	"province": "Metro Manila",
		// 	"country": "Philippines"
		// }
	// JSON Arrays
		// [
		// 	{
		// 		"city": "Quezon City",
		// 		"province": "Metro Manila",
		// 		"country": "Philippines"
		// 	},
		// 	{
		// 		"city": "Manila City",
		// 		"province": "Metro Manila",
		// 		"country": "Philippines"
		// 	}
		// ];


// [Section] JSON methods
	//JSON Object contains method for parsing and converting data into stringified JSON.

	let batchesArr = [
						{
							batchName: "Batch X"
						},
						{
							batchName: "Batch Y"
						}

					];
	// the stringify method is used to convert JS onjects into string
	console.log(batchesArr);
	console.log("Result from stringify method:");
	let stringBatchArr = JSON.stringify(batchesArr);
	console.log(stringBatchArr);
	// console.log(typeof stringBatchArr);  outputs string

	// we can also do it directly

	let placesArr = JSON.stringify([
							{
								batchCountry: "PH"
							},
							{
								batchCountry: "JP"
							}
	
						])
	console.log(placesArr);

// [section] using stringify method w/ variables
	
	// when information is stored in a variable and is not hard coded into an object that is being stringified, we can supply the value with a variable.

	// user detail

		// let firstName = prompt("What is your first name: ");
		// let lastName = prompt("What is your last name: ");
		// let age = prompt("Tell me you age: ");
		// let address = {
		// 	city: prompt("Which city do you live in? "),
		// 	country: prompt("Which country do you live in?")
		// }

		// let otherData = JSON.stringify({
		// 	firstName,
		// 	lastName,
		// 	age,
		// 	address
		// })

		// console.log(otherData);


// [section] Converting stringified JSON into JS Objects

	/*
		- objects are common data types used in application because of the complex datastructures that can be created out of them.
		- information is commonly sent to applications in stringified JSON and then converted back into objects.
		- this happnes both fir sending information to a backend application and sending information back to a frontend application.
	*/

	let batchesJSON = `[{"batchName":"Batch X"},{"batchName":"Batch Y"}]`;
	let parseBatchesJSON = JSON.parse(batchesJSON);
	console.log(parseBatchesJSON + " -- Parsed JSON");
	console.log(parseBatchesJSON[0]);

	let stingifiedObject = `{
		"name": "John",
		"age": "25",
		"address": {
			"city": "Manila",
			"country": "PH"
		}
	}`
	console.log(JSON.parse(stingifiedObject));